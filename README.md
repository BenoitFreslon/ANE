# ANE

## URLs
All ANE - Air Native Extensions

[http://www.adobe.com/devnet/air/native-extensions-for-air.html](http://www.adobe.com/devnet/air/native-extensions-for-air.html)

[http://sleepydesign.blogspot.fr/2012/07/ane-free-air-native-extensions.html](http://sleepydesign.blogspot.fr/2012/07/ane-free-air-native-extensions.html)

# SDKs
* [Chartboost](https://answers.chartboost.com/hc/en-us/articles/203473969-Adobe-AIR-Plugin-Download)
* [AdColony](https://github.com/AdColony/AdColony-AdobeAIR-SDK)
* [RevMob](http://sdk.revmobmobileadnetwork.com/air.html)

# Tips

# Flash
Use retina medias

# iOS

Export all Splash screens in the Root of the .fla file and add the files in AIR for iOS parameters > Files included

[How to name the Splash screens](http://help.adobe.com/en_US/air/build/WS901d38e593cd1bac1e63e3d129907d2886-8000.html#WS901d38e593cd1bac58d08f9112e26606ea8-8000)

# Android