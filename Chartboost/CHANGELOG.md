Adobe AIR Change Log
====================

Version 5.5.0 *(2015-06-28)*
----------------------------
>- *Android: Version 5.5.0*
>- *iOS: Version 5.5.0*

Features:
- Android: Fixed an issue with age gate appearing behind the ad display. 
- Post-install analytics now available for AIR. 

Fixes:
- Android: Video ads stop playing when didPauseClickForConfirmation() is called. 
- Android: Removed an unnecessary video prefetch call. 
- Android: Fixed Chartboost activity leaking in some cases.  
- Android: Loading screens were improperly showing for interstitial and rewarded videos.  
- Android: Typos fixed in some post install event responses. 
- Android: Timezone format fixed for Android 4.1 and early. 
- Android: Ads will no longer disappear on rotation if using Activities 
- Android: Reward video pre roll screen didn't show on cached calls. 
- Android: Fixed null pointer exception when initializing Chartboost and making calls on different threads. 
- Android: Fixed ads disappearing on rotation when using activities.  
- Android: Under certain circumstances, the SDK was failing to prefetch videos. 
- Android: Ad will no longer disappear in some devices if app is backgrounded 


- iOS: Phone calls during video ads caused video to pause with no way to resume.  
- iOS: The video prefetcher would sometimes delete the requested video causing the ad to fail.  
- iOS: MoreApps now correctly caches the next page on the first call instead of reusing the data. 
 
Improvements:
- iOS: Rewarded videos no longer need to wait until the video prefetcher is complete before showing an existing video. 
- iOS: AFNetworking library updated to version 2.5.4. 
- ChartboostExample UI layout that shows delegate logs, cache state, and allows the user to set various options including Show Age Gate. 
- Added CBClickError enums for didFailToRecordClick delegate. 
- Android:Added error logging for all network calls. 
- Sends the timezone and network type along with every request to be used by analytics and the ad server. 
- Sends the AIR runtime version in requests 


Version 5.2.0 *(2015-04-30)*
----------------------------
>- *Android: Version 5.3.0*
>- *iOS: Version 5.3.0*

Features:

- Added removeDelegateEvent functionality. 

Fixes:

- Android: Ad will no longer disappear in some devices if app is backgrounded 

Improvements:

- Added a way to detect rooted devices. 

Version 5.1.4 *(2015-03-23)*
----------------------------
>- *Android: Version 5.1.3*
>- *iOS: Version 5.1.5*

Features:

Fixes:

- Fix issue where cache and show calls were not firing immediatley after bootup for Android. 
- Fix no host activity error for Android. 

Improvements:

Version 5.1.3 *(2015-03-23)*
----------------------------
>- *Android: Version 5.1.3*
>- *iOS: Version 5.1.5*

Features:

Fixes:

Improvements:

- Changed the init() method of the AIR native extenstion to startWith().  This solves a symbol conflict for many users that do not use the hideAneLibSymbols or for users with specific build setups. 

Version 5.1.2 *(2015-03-13)*
----------------------------
>- *Android: Version 5.1.3*
>- *iOS: Version 5.1.5*

Features:

Fixes:

- Fix Seeing duplicate calls for showInterstitial on Unity Android. 
- Fix Seeing duplicate calls for showRewardedVideo on Unity Android. 
- Fix issue where close buttons for video were not working on startup. 

Improvements:

Version 5.1.1 *(2015-03-04)*
----------------------------
>- *Android: Version 5.1.2*
>- *iOS: Version 5.1.4*

Features:

- Added InPlay support. 
- Added a new method 'setStatusBarBehavior' to control how fullscreen video ads interact with the status bar. 

Fixes:

- Fix application force quiting on back pressed when no ads shown. 
- Fix didDismissInterstitial and didDismissRewardedVideo not executing on Android. 
- Fix incorrect debug message in CBManifestEditor.cs. 
- Fix for duplicate calls to the creative url from the SDK. This should fix issues with third party click tracking reporting click numbers twice what we report. 
- Fix for max ads not being respected when campaign is set to show once per hour and autocache is set to YES. There is now a small delay after successful show call. 
- Fix issue for interstitial video and rewarded video calling didDismissInterstitial or didDismissRewardedVideo during a click event. 
- Fix didCacheInterstitial not being called if an ad was already cached at the location. 
- Fix issue where close buttons for fullscreen video were appearing behing the status bar. 

Improvements:

- Added the location parameter, when available, to more relevant network requests. This should provide more information to analytics. 

Version 5.1.0 *(2015-01-16)*
----------------------------
>- *Android: Version 5.1.0*
>- *iOS: Version 5.1.3*

Features:

Fixes:

Improvements:

 - Updated to Android version 5.1.0 and iOS SDK versions 5.1.3.
 - Updated API completely, see the README.md.

Version 1.1.0
----------------------------

 - Fixed MoreApps delegate methods on iOS.
 - Updated build script to automatically download the correct version of the Adobe AIR SDK Compiler.
 - Included extension.xml and platform_ios.xml files in build folder for building the ANE.

Version 1.0.0
----------------------------

 - Initial release.

 
