# StageAd ANE Sample Application

Stage Ad sample application is an actionscript mobile project which allows the integration of iOS Banner or Fullscreen Ads in an application. Using this application various actions can be performed on an AD like change alignment, change viewport, set offset etc.

## Docs Link

For all the supported StageAd API's please refer the doc:http://www.adobe.com/devnet-docs/gamingsdk/anedocs/

## HOW TO IMPORT THE STAGEAD PROJECT IN FLASH BUILDER AND RUN ON THE MOBILE DEVICE:

1)Overlay the AIR SDK bundled with Gaming SDK in Flash Builder 4.7 or later. Please refer the doc for the same: http://helpx.adobe.com/flash-builder/kb/overlay-air-sdk-flash-builder.html

2)Import the StageAd project in Flash Builder(File>>Import Flash Builder Project) from:
   Mac: <Gaming SDK Installation Location>/Samples/Native Extensions (ANEs)/StageAd
   Win: <Gaming SDK Installation Location>\Samples\NativeExtension(ANE's)/StageAd

3)For running the project on mobile devices:
    -Right Click on the project select Run AS>>RunConfigurations.
    -Select launch method "On Device" and Specify the path of Provisioning profile and Certificates.p12 under "Configure package settings" link.
    -Apply the setting and run.

NOTE: On Windows,Either run the Flash Builder as an Admin(Right click>>Run as administrator) and import the project directly from Gaming SDK folder OR import the project in your workspace(File>>Import>>General>>Existing Project into Workspace) as "Program Files" folder is not writable and may leads to read only permission error on directly importing the project from Gaming SDK.