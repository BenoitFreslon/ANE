package
{	
	import com.adobe.ane.stageAd.*;
	import flash.display.Sprite;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	[SWF( backgroundColor="#CCCCFF")]
	public class StageAdTest extends Sprite
	{
		private var banner1:CustomButton = new CustomButton("Banner1",0,35,100,30);
		private var banner1Stage:CustomButton = new CustomButton("Stage",0,72,50,30);
		private var banner1Align:CustomButton = new CustomButton("Align",53,72,50,30);
		private var banner1Viewport:CustomButton = new CustomButton("Viewport",0,108,50,30);
		private var banner1Bye:CustomButton = new CustomButton("ByeBanner1",0,143,62,30);
		private var banner1ShowAd:CustomButton = new CustomButton("ShowAdT",64,143,53,30);
			
			
		private var banner2:CustomButton = new CustomButton("Banner2",230,35,100,30);
		private var banner2Stage:CustomButton = new CustomButton("Stage",230,72,50,30);
		private var banner2Align:CustomButton = new CustomButton("Align",283,72,50,30);
		private var banner2Viewport:CustomButton = new CustomButton("Viewport",283,108,50,30);
		private var banner2Bye:CustomButton = new CustomButton("ByeBanner2",220,143,62,30);
		private var banner2ShowAd:CustomButton = new CustomButton("ShowAdT",285,143,52,30);
			
		private var  fullScreen:CustomButton = new CustomButton("FullScreen",130,35,90,30);
		private var  fullscreenStage:CustomButton = new CustomButton("Stage",135,72,50,30);
		private var  clearLog:CustomButton = new CustomButton("ClearLog",135,143,50,30);
			
		private var bannerText:TextField = new TextField();
		private var banner1Offset:TextField = new TextField();
		private var banner2Offset:TextField = new TextField();
		
		private var ctf:CreateTextField=new CreateTextField();
		private var cb:CustomButton=new CustomButton();
		
		private var stageBannerAd1:StageBannerAd;
		private var stageBannerAd2:StageBannerAd;
		private var fullscreenAd:StageFullscreenAd;
		private var timer:Timer=new Timer(10000,1);
		
		public function StageAdTest()
		{
			
			addChild(banner1);
			addChild(banner1Stage);
			addChild(banner1Align);
			addChild(banner1Viewport);
			addChild(banner1Bye);
			addChild(banner1ShowAd);
			
			addChild(banner2);
			addChild(banner2Stage);
			addChild(banner2Align);
			addChild(banner2Viewport);
			addChild(banner2Bye);
			addChild(banner2ShowAd);
			
			addChild(fullScreen);
			addChild(fullscreenStage);
			addChild(clearLog);
			
			bannerText= ctf.customTextField(0,192,320,123);
			
			banner1Offset= ctf.customTextField(53,108,50,28);
			banner1Offset.type="input";
			banner1Offset.text="offset";
			banner2Offset= ctf.customTextField(230,108,50,28);
			banner2Offset.type="input";
			banner2Offset.text="offset";
			
			addChild(bannerText);
			addChild(banner1Offset);
			addChild(banner2Offset);
			
			addListeners();
		}
		
		private function addListeners():void
		{
			banner1.addEventListener(MouseEvent.CLICK,createBanner);
			banner1Stage.addEventListener(MouseEvent.CLICK,onBannerStageHandler);
			banner1Align.addEventListener(MouseEvent.CLICK,onBannerAlignHandler);
			banner1Viewport.addEventListener(MouseEvent.CLICK,onBannerViewPortHandler);
			banner1Bye.addEventListener(MouseEvent.CLICK,onByeBannerHandler);
			banner1ShowAd.addEventListener(MouseEvent.CLICK,onBannerShowAdHandler);
		    banner1Offset.addEventListener(FocusEvent.FOCUS_OUT,onBanner1OffsetHandler);
				
			banner2.addEventListener(MouseEvent.CLICK,createBanner);
			banner2Stage.addEventListener(MouseEvent.CLICK,onBannerStageHandler);
			banner2Align.addEventListener(MouseEvent.CLICK,onBannerAlignHandler);
			banner2Viewport.addEventListener(MouseEvent.CLICK,onBannerViewPortHandler);
			banner2Bye.addEventListener(MouseEvent.CLICK,onByeBannerHandler);
			banner2ShowAd.addEventListener(MouseEvent.CLICK,onBannerShowAdHandler);
			banner2Offset.addEventListener(FocusEvent.FOCUS_OUT,onBanner2OffsetHandler);
			
			fullScreen.addEventListener(MouseEvent.CLICK,createBanner);
			fullscreenStage.addEventListener(MouseEvent.CLICK,onBannerStageHandler);
			clearLog.addEventListener(MouseEvent.CLICK,onClearLogHandler);
		}
		
		protected function createBanner(event:MouseEvent):void
		{
			if(event.target==banner1)
			{
				if(StageBannerAd.isSupported)
				{
					bannerText.text+="\nbannerAd1.isSupported=true";
					stageBannerAd1=new StageBannerAd();
					bannerText.text+="\ndefault values of bannerAd1:\n align:"+stageBannerAd1.align+" offset:"+stageBannerAd1.offset+" showOnlyIfLoaded:"+stageBannerAd1.showOnlyIfLoaded+" stage:"+stageBannerAd1.stage;
					addAllEventListeners(stageBannerAd1);
				}
				else
				{
					bannerText.text+="\nbannerAd1.isSupported=false";
				}
			}
			else if(event.target==banner2)
			{
				if(StageBannerAd.isSupported)
				{
					bannerText.text+="\nbannerAd2.isSupported=true";
					stageBannerAd2=new StageBannerAd();
					bannerText.text+="\ndefault values of bannerAd1:\n align:"+stageBannerAd2.align+" offset:"+stageBannerAd2.offset+" showOnlyIfLoaded:"+stageBannerAd2.showOnlyIfLoaded+" stage:"+stageBannerAd2.stage;
					addAllEventListeners(stageBannerAd2);
				}
				else
				{
					bannerText.text+="\nbannerAd2.isSupported=false";
				}
			}
			else if(event.target==fullScreen)
			{
				if(StageFullscreenAd.isSupported)
				{
					bannerText.text+="\nFullscreenAd.isSupported=true";
					fullscreenAd=new StageFullscreenAd();
					addAllEventListeners1(fullscreenAd);
				}
				else
				{
					bannerText.text+="\nstageFullscreenAd.isSupported=false";
				}
			}
		}
		
		//Add all eventlisteners to bannerAd
		protected function addAllEventListeners(ad:StageBannerAd):void
		{
			ad.addEventListener(StageAdEvent.AD_ACTION_COMPLETE,onActionCompleteHandler);
			ad.addEventListener(StageAdEvent.AD_ACTION_START,onActionStartingHandler);
			ad.addEventListener(StageAdEvent.AD_LOAD_COMPLETE,onAdLoadCompleteHandler);
			ad.addEventListener(StageAdEvent.AD_LOAD_FAIL,onAdLoadFailHandler);
			ad.addEventListener(StageAdEvent.AD_UNLOAD,onAdUnloadHandler);
		}
		
		//Add all eventlisteners to fullscreenAd
		protected function addAllEventListeners1(ad:StageFullscreenAd):void
		{
			
			ad.addEventListener(StageAdEvent.AD_ACTION_COMPLETE,onActionCompleteHandler);
			ad.addEventListener(StageAdEvent.AD_ACTION_START,onActionStartingHandler);
			ad.addEventListener(StageAdEvent.AD_LOAD_COMPLETE,onAdLoadCompleteHandler);
			ad.addEventListener(StageAdEvent.AD_LOAD_FAIL,onAdLoadFailHandler);
			ad.addEventListener(StageAdEvent.AD_UNLOAD,onAdUnloadHandler);
		}
		
		public function onActionCompleteHandler(event:StageAdEvent):void
		{
			
			bannerText.text+="\naction complete handler properties:bubbles-"+event.bubbles+"cancelable:"+event.cancelable+"target:"+event.target+"type:"+event.type+"leaveApplication"+event.willLeaveApplication;
			
		}
		
		public function onActionStartingHandler(event:StageAdEvent):void
		{
			bannerText.text+="\naction starting handler properties:bubbles-"+event.bubbles+"cancelable:"+event.cancelable+"target:"+event.target+"type:"+event.type+"leaveApplication"+event.willLeaveApplication;
			
		}
		
		public function onAdLoadCompleteHandler(event:StageAdEvent):void
		{
			
			bannerText.text+="\nAD load handler properties:bubbles-"+event.bubbles+"cancelable:"+event.cancelable+"target:"+event.target+"type:"+event.type+"leaveApplication"+event.willLeaveApplication;
			//For checking the cancel action uncomment the below lines and the methods.
			/*bannerText.text+="\ntimer started";
			onCancelAction();*/ 
			
		}
	
		public function onAdLoadFailHandler(event:StageAdEvent):void
		{
			bannerText.text+="\naADLoadfail handler properties:bubbles-"+event.bubbles+"cancelable:"+event.cancelable+"target:"+event.target+"type:"+event.type+"leaveApplication"+event.willLeaveApplication;
			
		}
		
		public function onAdUnloadHandler(event:StageAdEvent):void
		{
			bannerText.text+="\nAdUnload handler properties:bubbles-"+event.bubbles+"cancelable:"+event.cancelable+"target:"+event.target+"type:"+event.type+"leaveApplication"+event.willLeaveApplication;
			
			
		}
		
		//Cancel the ACTION STarted on the Ads.Uncomment this for checking the cancel action API
		/*protected function onCancelAction():void
		{
		timer.start();
		timer.addEventListener(TimerEvent.TIMER,onTimerHandler);
		}
		
		public function onTimerHandler(event:TimerEvent):void
		{	
		
		stageBannerAd1.cancelAction();
		stageBannerAd2.cancelAction();
		fullscreenAd.cancelAction();
		bannerText.text+="\naction canceled";//
		}*/
		
		//Changes the property of stage to null/this.stage for banner and fullscreen Ads
		protected function onBannerStageHandler(event:MouseEvent):void
		{
			if(event.target==banner1Stage)
			{
				if(stageBannerAd1.stage==null)
				{
					stageBannerAd1.stage=this.stage;
					bannerText.text+="\nbannerAd1.stage=this.stage";
							
				}
					
					
				else
				{
					stageBannerAd1.stage=null;
					bannerText.text+="\nbannerAd1.stage=null";
					
				}
			}
			else if(event.target==banner2Stage)
			{
				if(stageBannerAd2.stage==null)
				{
					stageBannerAd2.stage=this.stage;
					bannerText.text+="\nbannerAd2.stage=this.stage";
					
				}
				else
				{
					stageBannerAd2.stage=null;
					bannerText.text+="\nbannerAd2.stage=null";
				
				}
			}
			else if(event.target==fullscreenStage)
			{
				fullscreenAd.addToStage();
				bannerText.text+="\nfullscreenAd added to stage";
				
			}
		}
		
		//Changes the property of align to top or bottom for banner and fullscreen Ads
		protected function onBannerAlignHandler(event:MouseEvent):void
		{
			if(event.target==banner1Align)
			{
				if(stageBannerAd1.align=="top")
				{
					
					stageBannerAd1.align=StageAdAlign.BOTTOM;
					bannerText.text+="\nbannerAd1.align=Bottom";
				
				}
				else
				{
					stageBannerAd1.align=StageAdAlign.TOP;
					bannerText.text+="\nbannerAd1.align=Top";
					
				}
			}
			else if(event.target==banner2Align)
			{
				if(stageBannerAd2.align=="top")
				{
					stageBannerAd2.align=StageAdAlign.BOTTOM;
					bannerText.text+="\nbannerAd2.align=Bottom";
					
				}
				else
				{
					stageBannerAd2.align=StageAdAlign.TOP;
					bannerText.text+="\nbannerAd2.align=Top";
					
				}
			}
		}
		
		//Changes the property of offset for banner1
		protected function onBanner1OffsetHandler(event:FocusEvent):void
		{
			trace(banner1Offset.text);
			stageBannerAd1.offset=uint(banner1Offset.text);
			bannerText.text+="\nbannerAd1.offset="+banner1Offset.text;
		}
		
		//Changes the property of offset for banner2
		protected function onBanner2OffsetHandler(event:FocusEvent):void
		{
			
			stageBannerAd2.offset=uint(banner2Offset.text);
			bannerText.text+="\nbannerAd2.offset="+banner2Offset.text;
		}
		
		//get the value of viewport for bannerAds
		protected function onBannerViewPortHandler(event:MouseEvent):void
		{
			if(event.target==banner1Viewport)
			{
				var viewportValue:Rectangle=stageBannerAd1.viewport;
				bannerText.text+="\nbannerAd1.viewport="+viewportValue;
			}
			else if(event.target==banner2Viewport)
			{
				bannerText.text+="\nbannerAd2.viewport="+stageBannerAd2.viewport;
			}
			
		}
		
		//Changes the property of showOnlyIfLoaded to true/false for bannerAds
		protected function onBannerShowAdHandler(event:MouseEvent):void
		{
			
			if(event.target==banner1ShowAd)
			{
				if(stageBannerAd1.showOnlyIfLoaded==true)
				{
					stageBannerAd1.showOnlyIfLoaded=false;
					bannerText.text+="\nbannerAd1.showOnlyIfLoaded=false now";
					
				}
				else
				{
					stageBannerAd1.showOnlyIfLoaded=true;
					bannerText.text+="\nbannerAd1.showOnlyIfLoaded=true now";
					
				}
			}
			if(event.target==banner2ShowAd)
			{
				if(stageBannerAd2.showOnlyIfLoaded==true)
				{
					stageBannerAd2.showOnlyIfLoaded=false;
					bannerText.text+="\nbannerAd2.showOnlyIfLoaded=false";
					
				}
				else
				{
					stageBannerAd2.showOnlyIfLoaded=true;
					bannerText.text+="\nbannerAd2.showOnlyIfLoaded=true";
					
				}
			}
		}
		
		//set stage to null for banner Ads.
		protected function onByeBannerHandler(event:MouseEvent):void
		{
			if(event.target==banner1Bye)
			{
				stageBannerAd1.stage=null;
				bannerText.text+="\nbye banner1";
				
			}
			else if(event.target==banner2Bye)
			{
				stageBannerAd2.stage=null;
				bannerText.text+="\nbye banner2";
				
			}
			
		}
		
		protected function onClearLogHandler(event:MouseEvent):void
		{
			bannerText.text=" ";
		}
		
	}
}
