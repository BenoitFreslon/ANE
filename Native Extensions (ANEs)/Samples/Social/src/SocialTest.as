/*************************************************************************
*
* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2012 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
**************************************************************************/

package
{
	import com.adobe.ane.social.SocialServiceType;
	import com.adobe.ane.social.SocialUI;
	
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;

	public class SocialTest extends Sprite
	{
		protected var apiSupportedLabel:TextField = new TextField();
		protected var msgLabel:TextField = new TextField();
		protected var urlLabel:TextField = new TextField();
		
		protected var apiSupportedField:TextField = new TextField();
		protected var msgField:TextField = new TextField();
		protected var urlField:TextField = new TextField();
		
		protected var fbButton:Sprite =  new Sprite();
		protected var tweetButton:Sprite = new Sprite();
		protected var swButton:Sprite =  new Sprite();
		protected var addImageButton:Sprite = new Sprite();
		
		protected var sUI:SocialUI;
		
		protected var msgFlag:Boolean;
		protected var urlFlag:Boolean;
		protected var addImage:Boolean=false;
		
		protected var bmd:BitmapData;
		
		public function SocialTest()
		{
			super();
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			createUI();
			
			apiSupportedField.text = SocialUI.isSupported.toString();	
		}
		
		public function createUI():void
		{
			apiSupportedLabel.text = "isSupported" ;
			apiSupportedLabel.visible= true;
			apiSupportedLabel.border = false;
			addChild(apiSupportedLabel);
			apiSupportedLabel.x=10;
			apiSupportedLabel.y=15;

			apiSupportedField.visible = true;
			apiSupportedField.border = true;
			apiSupportedField.x=120;
			apiSupportedField.y=10;
			this.addChild(apiSupportedField);
			apiSupportedField.height = 20;
			
			msgLabel.text = "Provide Message" ;
			msgLabel.visible= true;
			msgLabel.border = false;
			addChild(msgLabel);
			msgLabel.x=10;
			msgLabel.y=50;
			
			msgField.visible = true;
			msgField.border = true;
			msgField.x=120;
			msgField.y=50;
			msgField.type = "input";
			this.addChild(msgField);
			msgField.height = 20;		
			
			urlLabel.text = "Provide URL" ;
			urlLabel.visible= true;
			urlLabel.border = false;
			addChild(urlLabel);
			urlLabel.x=10;
			urlLabel.y=90;		
			
			urlField.visible = true;
			urlField.border = true;
			urlField.x=120;
			urlField.y=90;
			urlField.text="";
			urlField.type = "input";
			this.addChild(urlField);
			urlField.height = 20;
			
			var fbTextLabel:TextField = new TextField()
			fbButton.graphics.clear();
			fbButton.graphics.beginFill(0xD4D4D4); // grey color
			fbButton.graphics.drawRoundRect(10, 220, 110, 25, 10, 10); // x, y, width, height, ellipseW, ellipseH
			fbButton.graphics.endFill();
			fbTextLabel.text = "Launch FB ui";
			fbTextLabel.x = 12;
			fbTextLabel.y = 222;
			fbTextLabel.selectable = false;
			fbButton.addChild(fbTextLabel);
			
			this.addChild(fbButton);
			fbButton.addEventListener(MouseEvent.CLICK, launchFBUI);
			
			var tweetTextLabel:TextField = new TextField()
			tweetButton.graphics.clear();
			tweetButton.graphics.beginFill(0xD4D4D4); // grey color
			tweetButton.graphics.drawRoundRect(130, 220, 110, 25, 10, 10); // x, y, width, height, ellipseW, ellipseH
			tweetButton.graphics.endFill();
			tweetTextLabel.text = "Launch Twitter ui";
			tweetTextLabel.x = 132;
			tweetTextLabel.y = 222;
			tweetTextLabel.selectable = false;
			tweetButton.addChild(tweetTextLabel);
			tweetButton.addEventListener(MouseEvent.CLICK, launchTweetUI);
			
			this.addChild(tweetButton);
			
			var swTextLabel:TextField = new TextField()
			swButton.graphics.clear();
			swButton.graphics.beginFill(0xD4D4D4); // grey color
			swButton.graphics.drawRoundRect(250, 220, 120, 25, 10, 10); // x, y, width, height, ellipseW, ellipseH
			swButton.graphics.endFill();
			swTextLabel.text = "Launch sinaWeibo ui";
			swTextLabel.x = 252;
			swTextLabel.y = 222;
			swTextLabel.selectable = false;
			swButton.addChild(swTextLabel);
			swButton.addEventListener(MouseEvent.CLICK, launchswUI);
			
			this.addChild(swButton);
			
			var imageTextLabel:TextField = new TextField()
			addImageButton.graphics.clear();
			addImageButton.graphics.beginFill(0xD4D4D4); // grey color
			addImageButton.graphics.drawRoundRect(10, 120, 110, 25, 10, 10); // x, y, width, height, ellipseW, ellipseH
			addImageButton.graphics.endFill();
			imageTextLabel.text = "Add Image";
			imageTextLabel.x=12
			imageTextLabel.y = 124;
			imageTextLabel.selectable = false;
			addImageButton.addChild(imageTextLabel);
			
			this.addChild(addImageButton);
			addImageButton.addEventListener(MouseEvent.CLICK, select_image);
		}
		
		public function launchFBUI(event:MouseEvent):void
		{
			sUI = new SocialUI(SocialServiceType.FACEBOOK);
			msgFlag = sUI.setMessage(msgField.text);
			if(urlField.text!="")
		    urlFlag = sUI.addURL(urlField.text);
			
			if(addImage==true)
			sUI.addImage(bmd);
			
			sUI.launch();
		}
		
		public function launchTweetUI(event:MouseEvent):void
		{
			sUI = new SocialUI(SocialServiceType.TWITTER);
			msgFlag = sUI.setMessage(msgField.text);
			
			if(urlField.text!=" ")
				urlFlag = sUI.addURL(urlField.text);
			
			
			if(addImage==true)
				sUI.addImage(bmd);
			
			sUI.launch();
		}
		
		public function launchswUI(event:MouseEvent):void
		{
			sUI = new SocialUI(SocialServiceType.SINAWEIBO);
			msgFlag = sUI.setMessage(msgField.text);
			urlFlag = urlFlag = sUI.addURL(urlField.text);
			sUI.launch();
		}
		
		protected function select_image(event:MouseEvent):void{
			addImage= true;
			var loader:Loader =  new Loader();
			loader.load(new URLRequest("/assets/images.jpg"));
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE,onPicLoad)
		}
		
		
		protected  function onPicLoad(e:Event):void{
			var ld:LoaderInfo = LoaderInfo(e.target);
			trace(ld.toString());
			 bmd= new BitmapData(ld.width, ld.height, false, 0xFFFFFF);
			bmd.draw(ld.loader);
		}
	}
}