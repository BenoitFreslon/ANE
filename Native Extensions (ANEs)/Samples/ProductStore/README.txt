# Product Store ANE Sample

Demonstrates the use of all product store API's  which allows to perform the following functions purchase products,Check product details,Transaction history etc.

## Docs link

For all the supported ProductStore API's please refer the doc: http://www.adobe.com/devnet-docs/gamingsdk/anedocs/


## HOW TO IMPORT THE PRODUCT STORE PROJECT IN FLASH BUILDER AND RUN ON THE MOBILE DEVICE:

1)Overlay the AIR SDK bundled in Gaming SDK in Flash Builder 4.7 or later. Please refer the doc for the same: http://helpx.adobe.com/flash-builder/kb/overlay-air-sdk-flash-builder.html

2)Import the ProductStore project in Flash Builder(File>>Import Flash Builder Project) from:
   Mac: <Gaming SDK Installation Location>/Samples/Native Extensions (ANEs)/ProductStore
   Win: <Gaming SDK Installation Location>\Samples\NativeExtension(ANE's)/ProductStore

3)For running the project on mobile devices:
    -Right Click on the project select Run AS>>RunConfigurations.
    -Select launch method "On Device" and Specify the path of Provisioning profile and Certificates.p12 under "Configure package settings" link.
    -Apply the setting and run.

NOTE:
On Windows,Either run the Flash Builder as an Admin(Rightclick>>Run as administrator) and import the project directly from Gaming SDK folder OR import the project in your workspace(File>>Import>>General>>Existing Project into Workspace) as "Program Files" folder is not writable and may leads to read only permission error on directly importing the project from Gaming SDK.