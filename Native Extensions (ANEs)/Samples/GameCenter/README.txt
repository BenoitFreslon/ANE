# Game Center Sample Application

Demonstrates the use of all the GameCenter API's for performing many operations like authenticating a player, submitting and retrieving scores, reporting and resetting achievements, connecting and starting multiplayer matches et cetera.

## Docs Link:

For all the supported GameCenter API's please refer the doc: http://www.adobe.com/devnet-docs/gamingsdk/anedocs/


## HOW TO IMPORT THE PROJECT IN FLASH BUILDER AND RUN ON MOBILE DEVICES:

1)Overlay the AIR SDK bundled with Gaming SDK in Flash Builder 4.7 or later.Please refer the doc for the same: http://helpx.adobe.com/flash-builder/kb/overlay-air-sdk-flash-builder.html

2)Import the GameCenter project in Flash builder(File>>Import Flash Builder Project) from:
  Mac: <Gaming SDK Installation Location>/Samples/Native Extensions (ANEs)/GameCenter
  Win: <Gaming SDK Installation Location>\Samples\NativeExtension(ANE's)/GameCenter

3)For running the project on mobile devices:
    -Right Click on the project select Run AS>>RunConfigurations.
    -Select launch method "On Device" and specify the path of Provisioning profile and Certificates.p12 under "Configure package settings" link.
    -Apply the setting and run.

NOTE:
On Windows,Either run the Flash Builder as an Admin(Right click>>Run as administrator) and import the project directly from Gaming SDK folder OR import the project in your workspace(File>>Import>>General>>Existing Project into Workspace) as "Program Files" folder is not writable and may leads to read only permission error on directly importing the project from Gaming SDK folder.