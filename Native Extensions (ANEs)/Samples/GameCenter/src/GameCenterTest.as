/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright [first year code created] Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/

package
{
	import com.adobe.ane.gameCenter.GameCenterAchievement;
	import com.adobe.ane.gameCenter.GameCenterAchievementEvent;
	import com.adobe.ane.gameCenter.GameCenterAuthenticationEvent;
	import com.adobe.ane.gameCenter.GameCenterController;
	import com.adobe.ane.gameCenter.GameCenterFriendEvent;
	import com.adobe.ane.gameCenter.GameCenterLeaderboardEvent;
	import com.adobe.ane.gameCenter.GameCenterMatchEvent;
	import com.adobe.ane.gameCenter.GameCenterMatchInvite;
	import com.adobe.ane.gameCenter.GameCenterPlayer;
	import com.adobe.ane.gameCenter.GameCenterScore;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	
	
	
	public class GameCenterTest extends Sprite
	{
		private var gcController : GameCenterController;
		private var basicUI:CustomButton = new CustomButton("BasicAPITesting",0,-115,100,30);
		private var matchMakingUI:CustomButton  = new CustomButton("MatchVoiceAPITesting",230,-115,120,30);
		public var basicUIArray:Array= new Array();
		public var matchMakingArray:Array= new Array();
		public var ctf:CreateTextField= new CreateTextField();
		public var logText:TextField;
		public var UIlabel:TextField;
		public var requestLabel:TextField;
		public var data:ByteArray;
		
		//basicUIButtons
		public var leadershipView:CustomButton= new CustomButton("leadershipView",0,-80,100,30);
		public var AchievementView:CustomButton = new CustomButton("AchievementView",120,-80,100,30);
		public var friendRequestView:CustomButton = new CustomButton("friendRequestView",240,-80,100,30);
		public var requestLeadershipCategories:CustomButton = new CustomButton("LeadershipCategories",0,-20,100,30);
		public var requestAcheivements:CustomButton = new CustomButton("Acheivements",120,-20,100,30);
		public var requestScores:CustomButton = new CustomButton("Scores",240,-20,100,30);
		public var requestFriendList:CustomButton = new CustomButton("FriendList",360,-20,100,30);
		public var submitScore:CustomButton = new CustomButton("submitScore",0,40,100,30);
		public var submitAchievements:CustomButton = new CustomButton("submitAchievements",120,40,100,30);
		public var resetAchievements:CustomButton = new CustomButton("resetAchievements",240,40,100,30);
		public var clearLog:CustomButton = new CustomButton("ClearLog",360,40,100,30);
		public var removeImage:CustomButton = new CustomButton("removeImage",360,100,100,30);
		//MatchMakingButtons
		public var matchMakerView:CustomButton = new CustomButton("matchMakerView",0,-80,100,30);
		public var startMatch:CustomButton = new CustomButton("startMatch",120,-80,100,30);
		public var requestPlayers:CustomButton = new CustomButton("requestPlayers",240,-80,100,30);
		public var acceptMatch:CustomButton = new CustomButton("acceptMatch",0,-20,100,30);
		public var sendMatchData:CustomButton = new CustomButton("sendMatchData",120,-20,100,30);
		public var startMatchVoiceChannel:CustomButton = new CustomButton("startMatchVoiceChannel",0,-20,100,30);
		public var stopMatchVoiceChannel:CustomButton = new CustomButton("stopMatchVoiceChannel",120,-20,100,30);
		public var enableMicOnVoiceChannel:CustomButton = new CustomButton("enableMicOnVoiceChannel",0,40,100,30);
		public var disableMicOnVoiceChannel:CustomButton = new CustomButton("disableMicOnVoiceChannel",120,40,100,30);
		public var endMatch:CustomButton = new CustomButton("endMatch",240,-20,100,30);
		public var clearLog1:CustomButton = new CustomButton("ClearLog",240,40,100,30);
		private var gm:GameCenterMatchInvite;
		
		
		
		public function GameCenterTest():void
		{
			basicUI.addEventListener(MouseEvent.CLICK,setupBasicUI);
			matchMakingUI.addEventListener(MouseEvent.CLICK,setupMatchMaking);
			UIlabel=ctf.customTextField(0,-130,300,20,false);
			UIlabel.text="Select an API you want to test:";
			requestLabel = ctf.customTextField(0,-50,300,20,false);
			requestLabel.text="Request:"
			logText=ctf.customTextField(0,100,500,400);
			addChild(logText);
			addChild(basicUI);
			addChild(matchMakingUI);
			addChild(UIlabel);
			
			data =new ByteArray();
			data.writeInt(334);
			data.writeUTF("MatchData");
			data.position=0;
			
			if (GameCenterController.isSupported) {
				logText.text="gamecenter is supported";
				gcController = new GameCenterController();
				addListeners();
				if (!gcController.authenticated) {
					gcController.addEventListener(GameCenterAuthenticationEvent.PLAYER_AUTHENTICATED, authTrue);
					gcController.authenticate();
				}
			}
			else
			{
				logText.text="gamecenter is not supported";
			}
		}
		
		protected function authTrue(event:GameCenterAuthenticationEvent):void
		{
			logText.text+="\n"+GameCenterAuthenticationEvent.PLAYER_AUTHENTICATED;
			if(gcController.localPlayer!=null)
				logText.text+= "Localplayer:" + gcController.localPlayer.alias+"playerID:"+gcController.localPlayer.id 
					+ "playerIsFriend"+gcController.localPlayer.isFriend;
			else trace("null");
			
		}
		
		private function addListeners():void
		{
			
			//Authenticate 
			gcController.addEventListener(GameCenterAuthenticationEvent.PLAYER_NOT_AUTHENTICATED,authFailed);
			gcController.addEventListener(GameCenterAuthenticationEvent.PLAYER_AUTHENTICATION_CHANGED,authChanged);
			//Leadership
			gcController.addEventListener(GameCenterLeaderboardEvent.LEADERBOARD_VIEW_FINISHED,leadViewFinished);
			gcController.addEventListener(GameCenterLeaderboardEvent.LEADERBOARD_CATEGORIES_LOADED,leaderboardeCategoriesLoaded);
			gcController.addEventListener(GameCenterLeaderboardEvent.LEADERBOARD_CATEGORIES_FAILED,leaderboardeCategoriesFailed);
			//Achievements
			gcController.addEventListener(GameCenterAchievementEvent.ACHIEVEMENTS_VIEW_FINISHED,achViewFinished);
			gcController.addEventListener(GameCenterAchievementEvent.ACHIEVEMENTS_LOADED,achLoaded);
			gcController.addEventListener(GameCenterAchievementEvent.ACHIEVEMENTS_FAILED,achFailed);
			gcController.addEventListener(GameCenterAchievementEvent.SUBMIT_ACHIEVEMENT_SUCCEEDED,achSubmittedSuccess);
			gcController.addEventListener(GameCenterAchievementEvent.SUBMIT_ACHIEVEMENT_FAILED,achSubmitFailed);
			gcController.addEventListener(GameCenterAchievementEvent.RESET_ACHIEVEMENTS_SUCCEEDED,resetSuccess);
			gcController.addEventListener(GameCenterAchievementEvent.RESET_ACHIEVEMENTS_FAILED,resetUnsuccess);
			//FriendReuest
			gcController.addEventListener(GameCenterFriendEvent.FRIEND_REQUEST_VIEW_FINISHED,friendRequestViewFinished);
			gcController.addEventListener(GameCenterFriendEvent.FRIEND_LIST_LOADED,friendListLoaded);
			gcController.addEventListener(GameCenterFriendEvent.FRIEND_LIST_FAILED,friendListFailed);
			//scores
			gcController.addEventListener(GameCenterLeaderboardEvent.SUBMIT_SCORE_SUCCEEDED,submitScoreSucceed);
			gcController.addEventListener(GameCenterLeaderboardEvent.SUBMIT_SCORE_FAILED,submitScoreFailed);
			gcController.addEventListener(GameCenterLeaderboardEvent.SCORES_LOADED,requestedScoresLoaded);
			gcController.addEventListener(GameCenterLeaderboardEvent.SCORES_FAILED,requestedScoresFailed);
			//MatchMaker
			gcController.addEventListener(GameCenterMatchEvent.MATCH_STARTED,matchStarted);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_FAILED,matchFailed);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_CANCELLED,matchCancelled);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_PLAYER_CONNECTED,playerConnected);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_PLAYER_DISCONNECTED,playerDisconnected);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_PLAYERS_LOADED,playersLoaded);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_PLAYERS_FAILED,playerLoadingFailed);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_DATA_RECEIVED,dataRecieved);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_INVITE_RECEIVED,inviteRecieved);
			gcController.addEventListener(GameCenterMatchEvent.SEND_MATCH_DATA_FAILED,dataSendingFailed);
			gcController.addEventListener(GameCenterMatchEvent.SEND_MATCH_DATA_SUCCEEDED,dataSendingSuccess);
			gcController.addEventListener(GameCenterMatchEvent.MATCH_VOICE_FAILED,voiceFailed);
			
		}
		
		protected function voiceFailed(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_VOICE_FAILED;
		}
		
		protected function dataSendingSuccess(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.SEND_MATCH_DATA_SUCCEEDED;
			
		}
		
		protected function dataSendingFailed(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.SEND_MATCH_DATA_FAILED;
			
		}
		
		
		protected function inviteRecieved(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_INVITE_RECEIVED;
			
			gm = event.matchInvite;
			logText.text="matchInvite:"+event.matchInvite+"MatchPlayer: "+event.matchPlayers+"MAtchData:" +event.matchData;
			gcController.acceptMatch(gm,false);
		}
		
		protected function playerLoadingFailed(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_PLAYERS_FAILED;
		}
		
		
		protected function dataRecieved(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_DATA_RECEIVED;
			
			var ba :ByteArray = new ByteArray();
			ba=event.matchData;
			ba.position=0;
			logText.text+= "ByteArrayLength"+ba.length + "readIntData" + ba.readInt() + "readUTFData" + ba.readUTF();
			
		}
		
		protected function playersLoaded(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_PLAYERS_LOADED;
			for each ( var x :GameCenterPlayer in event.matchPlayers)
			{
				logText.text+="\nalias:"+x.alias + "playerID:" + x.id + "playerISFriend:" + x.isFriend;
			}
			gcController.sendMatchData(data,event.matchPlayers);
		}
		
		protected function playerDisconnected(event:Event):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_PLAYER_DISCONNECTED;
			
		}
		
		protected function playerConnected(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_PLAYER_CONNECTED;
			
		}
		
		protected function matchCancelled(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_CANCELLED;
			
		}
		
		protected function matchFailed(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_FAILED;
			
		}
		
		protected function matchStarted(event:GameCenterMatchEvent):void
		{
			logText.text+="\n"+GameCenterMatchEvent.MATCH_STARTED;
			
		}
		
		protected function requestedScoresFailed(event:GameCenterLeaderboardEvent):void
		{
			logText.text+="\n"+GameCenterLeaderboardEvent.SCORES_FAILED;
			
		}
		
		protected function requestedScoresLoaded(event:GameCenterLeaderboardEvent):void
		{
			logText.text+="\n"+GameCenterLeaderboardEvent.SCORES_LOADED;
			for each ( var x : GameCenterScore in event.scores)
			{
				logText.text+="\ncontent"+x.context + " DateReported: " + x.dateReported + "FormattedValue  " + x.formattedValue+ "Player  " 
					+ x.player + "Rank  " + x.rank + "Value  " + x.value;
			}
			
		}
		
		protected function submitScoreFailed(event:GameCenterLeaderboardEvent):void
		{
			logText.text+="\n"+GameCenterLeaderboardEvent.SUBMIT_SCORE_FAILED;
			
		}
		
		protected function submitScoreSucceed(event:GameCenterLeaderboardEvent):void
		{
			logText.text+="\n"+GameCenterLeaderboardEvent.SUBMIT_SCORE_SUCCEEDED;
			
		}
		
		protected function friendListFailed(event:GameCenterFriendEvent):void
		{
			
			logText.text="\n"+GameCenterFriendEvent.FRIEND_LIST_FAILED;
		}
		
		protected function friendListLoaded(event:GameCenterFriendEvent):void
		{
			logText.text+="\n"+GameCenterFriendEvent.FRIEND_LIST_LOADED;
			logText.text+="\nFriendListArray"+event.friendList;
			
		}
		
		protected function friendRequestViewFinished(event:GameCenterFriendEvent):void
		{
			
			logText.text+="\n"+GameCenterFriendEvent.FRIEND_REQUEST_VIEW_FINISHED;
		}
		
		protected function resetSuccess(event:GameCenterAchievementEvent):void
		{
			logText.text+="\n"+GameCenterAchievementEvent.RESET_ACHIEVEMENTS_SUCCEEDED;			
		}
		
		protected function resetUnsuccess(event:GameCenterAchievementEvent):void
		{
			logText.text+="\n"+GameCenterAchievementEvent.RESET_ACHIEVEMENTS_FAILED;
			
		}
		
		protected function achSubmitFailed(event:GameCenterAchievementEvent):void
		{
			logText.text+="\n"+GameCenterAchievementEvent.SUBMIT_ACHIEVEMENT_FAILED;
			
		}
		
		protected function achSubmittedSuccess(event:GameCenterAchievementEvent):void
		{
			logText.text+="\n"+GameCenterAchievementEvent.SUBMIT_ACHIEVEMENT_SUCCEEDED;
			
		}
		
		protected function achFailed(event:GameCenterAchievementEvent):void
		{
			logText.text+="\n"+GameCenterAchievementEvent.ACHIEVEMENTS_FAILED;
			
		}
		
		protected function achLoaded(event:GameCenterAchievementEvent):void
		{
			logText.text+="\n"+GameCenterAchievementEvent.ACHIEVEMENTS_LOADED;
			for each ( var x:GameCenterAchievement in event.achievements)
			{
				x.addEventListener(GameCenterAchievementEvent.ACHIEVEMENT_IMAGE_LOADED,achImageLoaded);
				x.addEventListener(GameCenterAchievementEvent.ACHIEVEMENT_IMAGE_FAILED,achImageFailed);
				x.requestImage();
				logText.text+= "\nAchieved Description"+x.achievedDescription + " Hidden " + x.hidden + " Identifier " + x.identifier + "MAxpoints  " + 
					x.maximumPoints + " Title " + x.title + " UnachievedDescription " + x.unachievedDescription;
			}
		}
		
		protected function achImageFailed(event:GameCenterAchievementEvent):void
		{
			logText.text+="\n"+GameCenterAchievementEvent.ACHIEVEMENT_IMAGE_FAILED;
			
		}
		
		protected function achImageLoaded(event:GameCenterAchievementEvent):void
		{
			logText.text+="\n"+GameCenterAchievementEvent.ACHIEVEMENT_IMAGE_LOADED;
			var bmp : Bitmap = new Bitmap(event.image);
			basicUIArray[12]=addChild(bmp);
			bmp.x=100;
			bmp.y=100;
			bmp.width=200;
			bmp.height=200;	
			basicUIArray[13]=addChild(removeImage);
			removeImage.addEventListener(MouseEvent.CLICK,removeImageHandler);
			
		}
		
		protected function removeImageHandler(event:MouseEvent):void
		{
			removeChild(basicUIArray[12]);
			removeChild(basicUIArray[13]);
			basicUIArray.length=basicUIArray.length-2;
			
		}
		
		protected function achViewFinished(event:GameCenterAchievementEvent):void
		{
			
			logText.text+="\n"+GameCenterAchievementEvent.ACHIEVEMENTS_VIEW_FINISHED;
		}
		
		protected function leaderboardeCategoriesFailed(event:GameCenterLeaderboardEvent):void
		{
			logText.text+="\n"+GameCenterLeaderboardEvent.LEADERBOARD_CATEGORIES_FAILED;
			
		}
		
		protected function leaderboardeCategoriesLoaded(event:GameCenterLeaderboardEvent):void
		{
			logText.text+="\n"+GameCenterLeaderboardEvent.LEADERBOARD_CATEGORIES_LOADED;
			for each(var x: String in event.leaderboardCategories)
			{
				logText.text+="\nLeaderBoard Categories:"+x;
			}
			
		}
		
		protected function leadViewFinished(event:GameCenterLeaderboardEvent):void
		{
			logText.text+="\n"+GameCenterLeaderboardEvent.LEADERBOARD_VIEW_FINISHED;
			
		}
		
		protected function authChanged(event:GameCenterAuthenticationEvent):void
		{
			
			logText.text+="\n"+GameCenterAuthenticationEvent.PLAYER_AUTHENTICATION_CHANGED;
		}
		
		protected function authFailed(event:GameCenterAuthenticationEvent):void
		{
			logText.text+="\n"+GameCenterAuthenticationEvent.PLAYER_NOT_AUTHENTICATED;
			
		}
		
		
		protected function setupMatchMaking(event:MouseEvent):void
		{
			trace("in MatchMakerUI ui setup");   	
			removeBasicUI();
			matchMakingArray[0]=addChild(matchMakerView);
			matchMakingArray[1]=addChild(startMatch);
			matchMakingArray[2]=addChild(endMatch);
			matchMakingArray[3]=addChild(startMatchVoiceChannel);
			matchMakingArray[4]=addChild(stopMatchVoiceChannel);
			matchMakingArray[5]=addChild(enableMicOnVoiceChannel);
			matchMakingArray[6]=addChild(disableMicOnVoiceChannel);
			matchMakingArray[7]=addChild(requestPlayers);
			matchMakingArray[8]=addChild(clearLog1);
			
			matchMakerView.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			startMatch.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			endMatch.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			acceptMatch.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			sendMatchData.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			startMatchVoiceChannel.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			stopMatchVoiceChannel.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			enableMicOnVoiceChannel.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			disableMicOnVoiceChannel.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			requestPlayers.addEventListener(MouseEvent.CLICK,matchMakerButtonHandler);
			clearLog1.addEventListener(MouseEvent.CLICK,clearLogHAndler);
		}
		
		protected function matchMakerButtonHandler(event:MouseEvent):void
		{
			if(event.target==matchMakerView)
				gcController.showMatchmakerView();
			else if(event.target==startMatch)
				gcController.startMatch();
			else if(event.target==endMatch)
				gcController.endMatch();
			else if(event.target==startMatchVoiceChannel)
				gcController.startMatchVoiceChannel("CH1");
			else if(event.target==stopMatchVoiceChannel)
				gcController.stopMatchVoiceChannel("CH1");
			else if(event.target==enableMicOnVoiceChannel)
				gcController.enableMicOnMatchVoiceChannel("CH1");
			else if(event.target==disableMicOnVoiceChannel)
				gcController.disableMicOnMatchVoiceChannel("CH1");
			else if(event.target==requestPlayers)
				gcController.requestMatchPlayers();
			
		}
		
		protected function setupBasicUI(event:MouseEvent):void
		{
			trace("in basic ui setup");
			removeMatchMakingUI();
			
			basicUIArray[0]=addChild(leadershipView);
			basicUIArray[1]=addChild(AchievementView);
			basicUIArray[2]=addChild(friendRequestView);
			basicUIArray[3]=addChild(requestLeadershipCategories);
			basicUIArray[4]=addChild(requestAcheivements);
			basicUIArray[5]=addChild(requestFriendList);
			basicUIArray[6]=addChild(requestScores);
			basicUIArray[7]=addChild(submitScore);
			basicUIArray[8]=addChild(submitAchievements);
			basicUIArray[9]=addChild(resetAchievements);
			basicUIArray[10]=addChild(clearLog);
			basicUIArray[11]=addChild(requestLabel);
			
			leadershipView.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			AchievementView.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			friendRequestView.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			matchMakerView.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			requestAcheivements.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			requestFriendList.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			requestScores.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			requestLeadershipCategories.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			submitScore.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			submitAchievements.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			resetAchievements.addEventListener(MouseEvent.CLICK,basicUIButtonHandler);
			clearLog.addEventListener(MouseEvent.CLICK,clearLogHAndler);
		}		
		
		protected function clearLogHAndler(event:MouseEvent):void
		{
			logText.text="";
			
		}
		
		
		protected function basicUIButtonHandler(event:MouseEvent):void
		{
			if(event.target==leadershipView)
				gcController.showLeaderboardView();
			else if(event.target==AchievementView)
				gcController.showAchievementsView();
			else if(event.target==matchMakerView)
				gcController.showMatchmakerView();
			else if(event.target==friendRequestView)
				gcController.showFriendRequestView();
			else if(event.target==requestAcheivements)
				gcController.requestAchievements();
			else if(event.target==requestFriendList)
				gcController.requestFriendList();
			else if(event.target==requestScores)
				gcController.requestScores();
			else if(event.target==requestLeadershipCategories)
				gcController.requestLeaderboardCategories();
			else if(event.target==submitScore)
				gcController.submitScore(100);
			else if(event.target==submitAchievements)
				gcController.submitAchievement("myAchievement2",100);
			else if(event.target==resetAchievements)
				gcController.resetAchievements();
			
		}
		
		private function removeMatchMakingUI():void
		{
			if(matchMakingArray.length !=0)
			{
				for (var child in matchMakingArray)
					removeChild(matchMakingArray[child]);
				
			}
			
		}		
		private function removeBasicUI():void
		{
			trace(basicUIArray.length);
			if(basicUIArray.length !=0)
			{
				for (var child in basicUIArray)
					removeChild(basicUIArray[child]);
				
			}
			
		}	
		
		
		
	}
}